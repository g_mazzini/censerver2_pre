package it.eggzero.centricserver.security;

import it.eggzero.centricserver.model.CouchDbRepository;
import it.eggzero.centricserver.model.CouchDbSafeRepository;
import it.eggzero.centricserver.model.UserManagement;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PinHeaderFilter extends OncePerRequestFilter {

    @Autowired
    private CouchDbSafeRepository safeRepository;

    @Autowired
    private CouchDbRepository couchDbRepository;

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException, UsernameNotFoundException {
        String pinHeader = request.getHeader("Cookie");
        if (pinHeader != null && pinHeader.startsWith("Alternates")) {
            String pin = extraxtPinFromHeader(pinHeader);
            UserManagement user = safeRepository.getUserManagementByPIN(pin);
            if(user == null) {
                throw new UsernameNotFoundException("User not found");
            }
            boolean ok = userDetailService.checkExpiration(couchDbRepository.getCentricUserById(user.getUserId()));
            if(ok) {
                HttpRequestWithPin httpRequestWithPin = new HttpRequestWithPin(request, user.getUserId());
                filterChain.doFilter(httpRequestWithPin, response);
            } else {
                throw new UsernameNotFoundException("utente scaduto");
            }
        } else {
            filterChain.doFilter(request,response);
        }
    }

    private String extraxtPinFromHeader(String pinHeader) {
        String[] strings = pinHeader.split(";");
        if(strings != null) {
            String alternates = strings[0];
            if(alternates.equals("")) {
                return null;
            } else {
                String pin = alternates.substring(11, alternates.length());
                return pin;
            }
        }
        return null;
    }
}
