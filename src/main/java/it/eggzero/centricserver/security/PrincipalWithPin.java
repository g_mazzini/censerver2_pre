package it.eggzero.centricserver.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

import java.security.Principal;

public class PrincipalWithPin extends OAuth2Authentication {
    private String principalUsername;

    public PrincipalWithPin(OAuth2Request storedRequest, Authentication userAuthentication, String newPrincipalUsername) {
        super(storedRequest, userAuthentication);
        principalUsername = newPrincipalUsername;
    }

    @Override
    public Object getPrincipal() {
        return new Principal() {
            @Override
            public String getName() {
                return principalUsername;
            }
        };
    }
}
