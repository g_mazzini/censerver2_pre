package it.eggzero.centricserver.security;

import it.eggzero.centricserver.model.CouchDbSafeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;


@Service
@Primary
public class ClientDetailServiceImpl implements ClientDetailsService{

    @Value("${client.credentials.secret}")
    private String clientSecret;

    @Autowired
    private CouchDbSafeRepository couchDbSafeRepository;

    @Override
    public ClientDetails loadClientByClientId(String client_id) throws ClientRegistrationException {
        if(couchDbSafeRepository.isThisClientOK(client_id)) {
            BaseClientDetails clientDetails = new BaseClientDetails();
            clientDetails.setClientId(client_id);
            clientDetails.setClientSecret(clientSecret);
            Set<String> grantTypes = new HashSet<>();
            grantTypes.add("password");
            clientDetails.setAuthorizedGrantTypes(grantTypes);
            clientDetails.setAccessTokenValiditySeconds(60*60*24);
            return  clientDetails;
        }
        return null;
    }
}
