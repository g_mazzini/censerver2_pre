package it.eggzero.centricserver.security;

import it.eggzero.centricserver.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Service
@Primary
public class UserDetailServiceImpl implements UserDetailsService{

    @Autowired
    private CouchDbSafeRepository safeRepository;

    @Autowired
    private CouchDbRepository couchDbRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserManagement userManagement = safeRepository.getByUserManagementId(username);
        if(userManagement == null) {
            return null;
        } else {
            CentricUser centricUser = couchDbRepository.getCentricUserById(username);
            boolean active = checkExpiration(centricUser);
            String password = userManagement.getRegistrationPassword();
            String pin = userManagement.getPin();
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            User myUser = new User(username, password, active, active, active, active, grantedAuthorities);
            return myUser;
        }
    }

    public boolean checkExpiration(CentricUser user) {
        if(user != null) {
            if((user.getAdmissionStartDate() != null) && (user.getAdmissonEndDate() == null || user.getAdmissonEndDate().after(new Date()))) {
                return true;
            }
        }
        return false;
    }
}
