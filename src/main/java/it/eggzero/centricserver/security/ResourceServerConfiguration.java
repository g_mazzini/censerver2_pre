package it.eggzero.centricserver.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().authenticated().and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    /**
     * Filter added to the filterChain after the OAuth2AuthenticationProcessingFilter.
     * It filters the requests from users with PIN
     * @return
     */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public PinHeaderFilter pinHeaderFilter() {
        return new PinHeaderFilter();
    }
}