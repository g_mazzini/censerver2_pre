package it.eggzero.centricserver.security;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import javax.servlet.http.*;
import java.security.Principal;

public class HttpRequestWithPin extends HttpServletRequestWrapper {

    private String realUser;

    public HttpRequestWithPin(HttpServletRequest request, String newPrincipalName) {
        super(request);
        realUser = newPrincipalName;
    }

    @Override
    public Principal getUserPrincipal() {
        OAuth2Authentication oldPrincipal = (OAuth2Authentication) super.getUserPrincipal();
        PrincipalWithPin newPrincipal = new PrincipalWithPin(oldPrincipal.getOAuth2Request(), oldPrincipal.getUserAuthentication(), realUser);
        return newPrincipal;
    }
}
