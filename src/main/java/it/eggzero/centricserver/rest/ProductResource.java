package it.eggzero.centricserver.rest;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/products")
public class ProductResource {

    @Autowired
    private TokenStore tokenStore;

    @GetMapping
    @PreAuthorize("#oauth2.hasScope('centric')")
    public List<ResourceProperty> list(HttpServletRequest req, HttpServletResponse res, OAuth2Authentication auth2Authencation) {
        Principal principal = req.getUserPrincipal();
        System.out.println("API chiamata da = "+req.getUserPrincipal().getName());
        return Arrays.asList(new ResourceProperty("Caffè buono", 12.99), new ResourceProperty("Caffè meno buono", 4.21));
    }

    public static class ResourceProperty {

        private String name;
        private Object value;

        public ResourceProperty(String name, Object value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

    }
}
