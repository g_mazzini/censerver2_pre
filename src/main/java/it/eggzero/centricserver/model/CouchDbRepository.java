package it.eggzero.centricserver.model;

import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CouchDbRepository extends CentricBaseRepository {

    protected CouchDbRepository(Class<CouchDbDocument> type, CouchDbConnector db) {
        super(type, db);
    }

    public CentricUser getCentricUserById(String id) {

        return (CentricUser) giveMeTheOne("_design/Users", "cenAll", id, CentricUser.class);
    }

 }
