package it.eggzero.centricserver.model;

import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.ektorp.spring.HttpClientFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class CouchDbConnectorFactory {
    @Value("${db.url}")
    private String couchDbUrl;

    @Value("${db.shared.name}")
    private String couchDbSharedName;
    @Value("${db.shared.admin.username}")
    private String couchDbSharedAdminName;
    @Value("${db.shared.admin.password}")
    private String couchDbSharedAdminPassword;

    @Value("${client.db.password}")
    private String couchDbSharedCurrentPassword;


    @Value("${db.safe.admin.username}")
    private String couchDbSafeAdminName;
    @Value("${db.safe.admin.password}")
    private String couchDbSafeAdminPassword;
    @Value("${db.safe.name}")
    private String couchDbSafeName;

    public StdCouchDbConnector getCouchDbConnector() throws Exception {

        return new StdCouchDbConnector(couchDbSharedName, createDbInstance(false));
    }

    public StdCouchDbConnector getSafeConnector() throws Exception {

        return new StdCouchDbConnector(couchDbSafeName, createDbInstance(true));
    }

    public String getCurrentDbPwd() {
        return this.couchDbSharedCurrentPassword;
    }

    private CouchDbInstance createDbInstance(boolean safe) throws Exception {
        Properties properties = new Properties();
        properties.setProperty("autoUpdateViewOnChange", "true");

        HttpClientFactoryBean factory = new HttpClientFactoryBean();
        factory.setUrl(couchDbUrl);
        factory.setUsername((safe) ? couchDbSafeAdminName : couchDbSharedAdminName);
        factory.setPassword((safe) ? couchDbSafeAdminPassword : couchDbSharedAdminPassword);
        factory.setProperties(properties);
        factory.afterPropertiesSet();
        HttpClient client = factory.getObject();

        return new StdCouchDbInstance(client);
    }
}
