package it.eggzero.centricserver.model;

import org.ektorp.CouchDbConnector;
import org.ektorp.ViewQuery;
import org.ektorp.support.CouchDbDocument;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CouchDbSafeRepository extends CentricBaseRepository {

    protected CouchDbSafeRepository(Class<CouchDbDocument> type, CouchDbConnector db) {
        super(type, db);
    }

    public boolean isThisClientOK(String client_id) {
        ViewQuery query = new ViewQuery()
                .designDocId("_design/CentricApps")
                .viewName("cenActiveApps")
                .key(client_id);
        List<String> centricAppList = db.queryView(query, String.class);
        if(centricAppList.size() == 1 && centricAppList.get(0).equals("active")) {
            return true;
        }
        return false;
    }

    public UserManagement getByUserManagementId(String userId) throws UsernameNotFoundException {

        return (UserManagement) giveMeTheOne("_design/UserManagement", "cenViewByUserId", userId, UserManagement.class);
    }

    public UserManagement getUserManagementByPIN(String pin) {

        return (UserManagement) giveMeTheOne("_design/UserManagement", "cenViewByPin", pin, UserManagement.class);
    }

}