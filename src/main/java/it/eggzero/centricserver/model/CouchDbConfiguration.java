package it.eggzero.centricserver.model;


import org.ektorp.support.CouchDbDocument;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CouchDbConfiguration {

    @Bean
    public CouchDbConnectorFactory dbConnectorFactory() {
        return new CouchDbConnectorFactory();
    }

    @Bean
    CouchDbRepository couchDbRepository() throws Exception {
        return new CouchDbRepository(couchDbDocumentClass(), dbConnectorFactory().getCouchDbConnector());
    }

    @Bean
    CouchDbSafeRepository couchDbSafeRepository() throws Exception {
        return new CouchDbSafeRepository(couchDbDocumentClass(), dbConnectorFactory().getSafeConnector());
    }

    @Bean
    Class<CouchDbDocument> couchDbDocumentClass() {
        return CouchDbDocument.class;
    }
}

