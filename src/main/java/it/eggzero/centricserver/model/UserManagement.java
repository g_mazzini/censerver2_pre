package it.eggzero.centricserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.ektorp.support.CouchDbDocument;
import org.ektorp.support.TypeDiscriminator;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
public class UserManagement extends CouchDbDocument implements Serializable {

    @JsonIgnoreProperties(ignoreUnknown = true)
    @TypeDiscriminator
    @JsonProperty("type")
    private String type;
    @JsonProperty("sessions")
    private Set<Map<String, Object>> sessions;
    @JsonProperty("registrationEmail")
    private String registrationEmail;
    @JsonProperty("registrationPassword")
    private String registrationPassword;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("multiUserAppPIN")
    private String pin;
    @JsonProperty("controlConfirmationPIN")
    private String controlConfirmationPIN;
    @JsonProperty("personalMediaFeeds")
    private Set<Map<String, String>> personalMediaFeeds;
    @JsonProperty("sipAccount")
    private Map<String, Object> sipAccountData;
}

