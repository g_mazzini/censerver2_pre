package it.eggzero.centricserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.ektorp.support.CouchDbDocument;
import org.ektorp.support.TypeDiscriminator;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
public class CentricApp extends CouchDbDocument implements Serializable {

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("uuid")
    private String centricUUID;
    @TypeDiscriminator
    @JsonProperty("type")
    private String type;
    @JsonProperty("hostPlatform")
    private String hostPlatform;
    @JsonProperty("hostPlatformVendor")
    private String hostPlatformVendor;
    @JsonProperty("multiUserApp")
    private Map<String, Object> multiUserAppType;

/*-------------------------------- utils -----------------------------*/

    public boolean isMultiUserApp() {
        return !(multiUserAppType == null);
    }
}
