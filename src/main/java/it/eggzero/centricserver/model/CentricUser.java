package it.eggzero.centricserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.ektorp.support.CouchDbDocument;
import org.ektorp.support.TypeDiscriminator;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class CentricUser extends CouchDbDocument implements Serializable {

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("preferenceIds")
    private List<String> appPreferences;
    @JsonProperty("isAnonymous")
    private boolean isAnonymous;
    @JsonProperty("group")
    private String group;
    @JsonProperty("type")
    @TypeDiscriminator
    private String type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("centricName")
    private String centricName;
    @JsonProperty("surname")
    private String surname;
    @JsonProperty("locales")
    private Set<String> locales;
    @JsonProperty("mobileNumber")
    private java.lang.String mobileNumber;
    @JsonProperty("email")
    private String email;
    @JsonProperty("admissionStart")
    private Date admissionStartDate;
    @JsonProperty("admissionEnd")
    private Date admissonEndDate;
    @JsonProperty("created")
    private Date createdDate;
    @JsonProperty("modified")
    private Date modifiedDate;
}
