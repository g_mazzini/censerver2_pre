package it.eggzero.centricserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.ektorp.support.CouchDbDocument;
import org.ektorp.support.TypeDiscriminator;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

@Getter
@Setter
public class CentricAppManagement extends CouchDbDocument implements Serializable {

    @JsonIgnoreProperties(ignoreUnknown = true)
    @TypeDiscriminator
    @JsonProperty("type")
    private String type;
    @JsonProperty("uuid")
    private String centricUUID;
    @JsonProperty("activationCode")
    private String activationCode;
    @JsonProperty("activationCodeExpiry")
    private Date activationCodeExpiry;
    @JsonProperty("active")
    private boolean isActive;
    @JsonProperty("pushNotificationCoordinates")
    private Map<String, String> pushNotificationCoordinates;
}
