package it.eggzero.centricserver.model;

import org.ektorp.CouchDbConnector;
import org.ektorp.ViewQuery;
import org.ektorp.support.CouchDbDocument;
import org.ektorp.support.CouchDbRepositorySupport;

import java.util.List;

public class CentricBaseRepository<T extends CouchDbDocument> extends CouchDbRepositorySupport<CouchDbDocument> {

    public CentricBaseRepository(Class<CouchDbDocument> type, CouchDbConnector db) {
        super(type, db);
    }

    protected T giveMeTheOne(String designDoc, String viewName, String key, Class<T> myClass) {
        ViewQuery query = new ViewQuery()
                .designDocId(designDoc)
                .viewName(viewName)
                .key(key);
        List<T> dbDocuments = db.queryView(query, myClass);
        if(dbDocuments.size() == 1) {
            T myUniqueDocument = dbDocuments.get(0);
            return myUniqueDocument;
        } else {
            return null;
        }
    }
}
